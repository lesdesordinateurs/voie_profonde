import numpy as np                        # fundamental package for scientific computing
import matplotlib.pyplot as plt           # 2D plotting library producing publication quality figures
import pyrealsense2 as rs                 # Intel RealSense cross-platform open-source API
print("Environment Ready")

# Setup:
pipe = rs.pipeline()
#cfg = rs.config()
#excfg.enable_device_from_file("stairs.bag")
#profile = pipe.start(cfg)
profile = pipe.start()

# Skip 5 first frames to give the Auto-Exposure time to adjust
for x in range(1):
  pipe.wait_for_frames()
  
# Store next frameset for later processing:
frameset = pipe.wait_for_frames()
arrays_depth = np.empty([0,480,640])
for h in range(100):
  frameset = pipe.wait_for_frames()
  depth_frame = frameset.get_depth_frame()
  arrays_depth = np.append(arrays_depth,np.expand_dims(np.asanyarray(depth_frame.get_data()),axis=0),axis=0)

#print(np.ndim(arrays_depth))
average_depth = np.average(arrays_depth,axis=0)
print(np.asanyarray(depth_frame.get_data()).shape)
print(arrays_depth.shape)

# Cleanup:
pipe.stop()
print("Frames Captured")

#print(np.asanyarray(depth_frame.get_data()))
plt.subplot(1,2,1)
plt.rcParams["axes.grid"] = False
plt.rcParams['figure.figsize'] = [8, 4]
plt.imshow(np.asanyarray(depth_frame.get_data()))
plt.subplot(1,2,2)
plt.rcParams["axes.grid"] = False
plt.rcParams['figure.figsize'] = [8, 4]
plt.imshow(average_depth)

plt.show()

