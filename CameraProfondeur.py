import cv2
import numpy as np                        # fundamental package for scientific computing
import matplotlib.pyplot as plt           # 2D plotting library producing publication quality figures
import pyrealsense2 as rs                 # Intel RealSense cross-platform open-source API
print("Environment Ready")

# Setup:
pipe = rs.pipeline()
#cfg = rs.config()
#profile = pipe.start(cfg)
profile = pipe.start()

# Skip 10 first frames to give the Auto-Exposure time to adjust
for x in range(10):
  pipe.wait_for_frames()
  
# Averave the captured image:
frameset = pipe.wait_for_frames()
arrays_depth = np.empty([0,480,640])
for h in range(100):
  frameset = pipe.wait_for_frames()
  depth_frame = frameset.get_depth_frame()
  arrays_depth = np.append(arrays_depth,np.expand_dims(np.asanyarray(depth_frame.get_data()),axis=0),axis=0)

# Cleanup:
pipe.stop()

# Averaging:
average_depth = np.average(arrays_depth,axis=0)

print("Frames Captured")

# Contour detection:
aux1=cv2.GaussianBlur(average_depth,(3,3),0)
avmax = np.amax(aux1)
print(avmax)
avmin = np.amin(aux1)
print(avmin)

average_depth_norm=np.multiply(np.true_divide(average_depth,avmax),255)
aux3=np.median(average_depth_norm)
print(aux3)

# Detect contours
canny1=cv2.Canny(average_depth_norm.astype('uint8'),50,300)

#print(np.asanyarray(depth_frame.get_data()))
plt.subplot(1,2,1)
plt.rcParams["axes.grid"] = False
plt.rcParams['figure.figsize'] = [8, 4]
#plt.imshow(np.asanyarray(depth_frame.get_data()))
plt.imshow(average_depth_norm)
plt.subplot(1,2,2)
plt.rcParams["axes.grid"] = False
plt.rcParams['figure.figsize'] = [8, 4]
#plt.imshow(average_depth)
plt.imshow(canny1)

plt.show()

